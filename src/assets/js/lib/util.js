/**
 * Indiana Jones Travel By Map
 *
 * @namespace INDY
 * @module UTIL
*/

define([], function () {

  var UTIL = {};

  UTIL.selectIndex = 0;

  UTIL.selectField = function () {
    var index = UTIL.selectIndex || 0,
      input = document.querySelector('#journey').querySelectorAll('div input')[index];
    
    if (!input) {
      return;
    }
    
    if(input.hasOwnProperty('selectionStart')) {
      input.selectionStart = 0;
      input.selectionEnd = 0;
    }
    input.focus();
  };

  UTIL.optimalSetting = function () {
    var msg = document.createElement('p');
    msg.className = 'optimal-setting';
    msg.innerHTML = 'Please turn device around to portrait for it to work better.';
    document.querySelector('body').appendChild(msg);
  };

  /**
   * @method capitalize
   * @description Prototype of built-in String
   * @return String with the first character in uppercase
  */
  String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
  };

  return UTIL;

});