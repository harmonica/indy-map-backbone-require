requirejs.config({
  
  baseUrl: 'assets/js',
  
  shim: {
    underscore: {
      exports: '_'
    },
    backbone: {
      deps: [
        'underscore',
        'jquery'
      ],
      exports: 'Backbone'
    }
  },

  paths: {
    backbone: 'lib/vendor/backbone',
    domReady: 'lib/vendor/domReady',
    jquery: 'lib/vendor/jquery',
    modernizr: 'lib/vendor/modernizr-2.6.2.min',
    templates: 'templates',
    text: 'lib/vendor/text',
    underscore: 'lib/vendor/underscore.min'
  }
});

require(
  [
    'domReady',
    'modernizr',
    'backbone',
    'views/appView'
  ], function (domReady, modnzr, Backbone, AppView) {
    domReady(function () {
      new AppView();
    });
});

define("main", function (){});
