/*jslint indent: 2, vars: true, passfail: false */
/*globals INDY, INDYmapStyle, google, document, window, event, alert, console, setTimeout */

/**
 * The main application code. See Google API for help: https://developers.google.com/maps/documentation/javascript/overlays
 * @namespace INDY
 * @class app
*/
define([
  'backbone',
  'collections/locationCollection',
  'views/locationView',
  'views/drawView',
  'text!templates/app.html',
  'Config',
  'lib/util',
  'lib/gmap'
], function (Backbone, Journey, LocationView, DrawView, appTemplate, Config, Util, GMAP) {
    'use strict';

    Config.updatingModel = false;

    var AppView = Backbone.View.extend({

      el: '#' + Config.id, // Bind to an existing dom element

      template: _.template(appTemplate),

      events: {
        'click .build-route': 'build',
        'click .reset-journey': 'reset',
        'route-ready': 'draw'
      },

      initialize: function () {
        //console.log('Initializing AppView');
        this.render();
        GMAP.initializeGmap();
        this.listenTo(Journey, 'add', this.add);
        this.listenTo(Journey, 'reset', this.addAll);
        this.listenTo(Journey, 'change', this.checkForPending);
        this.reset();
      },

      render: function () {
        //console.log('Rendering AppView');
        this.$el.append(this.template);
      },

      waitingToDraw: false,

      checkForPending: function () {
        //console.log('checkForPending');
        Config.updatingModel = false;
        if (this.waitingToDraw) {
          this.waitingToDraw = false;
          this.build();
        }
      },

      add: function (location) {
        var view = new LocationView({model: location}),
          html = view.render().el,
          index = Journey.indexOf(location),
          journeyElem = $('#journey');

        if (journeyElem.find('div').length > index) {
          journeyElem.find('div:eq(' + (index - 1) + ')').after(html);
        } else {
          journeyElem.append(html);
        }

        var autocomplete = new google.maps.places.Autocomplete($(html).find('input')[0]);
        autocomplete.bindTo('bounds', map);
      },

      // Add all items in the Journey collection at once.
      addAll: function () {
        $('#journey').empty();
        Journey.each(this.add, this);
        Util.selectField();
      },

      reset: function () {
        Journey.reset(Config.defaultLocations);
        //DRAW.clearCanvas();
      },

      build: function () {
        var center,
          latlng,
          pixels,
          unsavedModels;

        if (Config.updatingModel) {
          this.waitingToDraw = true;
          //console.log('waiting to draw');
          return;
        }

        //DRAW.clearCanvas(); // Clear existing map

        unsavedModels = Journey.where({status: 'editing'});

        _.each(unsavedModels, function (model) {
          model.destroy();
        });

        _.each(Journey.models, function (model) {
          latlng = model.get('latlng');
          center = GMAP.center(latlng[0], latlng[1]),
          pixels = GMAP.pixelPosition(center);
          model.set({'x': pixels.x, 'y': pixels.y}, {silent: true});
        });
        this.$el.trigger('route-ready');
      },

      draw: function () {
        INDY.draw = new DrawView({model: Journey});
      }

    });

    return AppView;
  });
