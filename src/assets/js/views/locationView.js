define([
  'collections/locationCollection',
  'lib/util',
  'Config',
  'text!templates/location.html'
  ], function (Journey, Util, Config, locationTemplate) {
    'use strict';

    var LocationView = Backbone.View.extend({

      tagName: 'div',

      template: _.template(locationTemplate),

      events: {
        'blur input': 'update',
        'click .add-location': 'add',
        'click .remove-location': 'deleteModel',
        'remove-location': 'deleteModel',
        'click .clear-field': 'clear'
      },

      initialize: function () {
        this.listenTo(this.model, 'destroy', this.removeView);
        this.listenTo(this.model, 'change', this.render);
      },

      render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        this.$input = this.$('input');
        return this;
      },

      add: function (location) {
        var index = Journey.indexOf(this.model) + 1;
        Util.selectIndex = index;
        Journey.add({}, {at: index});
      },

      deleteModel: function () {
        console.log('destroy location model');
        this.model.destroy({});
        this.remove();
      },

      update: function () {
        var location = this.$input.val().trim();
        if (location !== '' && location !== this.model.get('name')) {
          Config.updatingModel = true; // Set flag to stop route being drawn
          this.model.set({status: 'editing'}, {silent: true});
          this.model.geoCode(location);
        }
      },

      removeView: function () {
        console.log('remove view');
        this.remove();
      },

      clear: function () {
        this.model.set({name: ''});
        this.$input.focus();
      }

    });

    return LocationView;
});