YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "INDY.GMap",
        "INDY.app",
        "INDY.config",
        "INDY.mapstyle"
    ],
    "modules": [
        "UTIL"
    ],
    "allModules": [
        {
            "displayName": "UTIL",
            "name": "UTIL",
            "description": "Indiana Jones Travel By Map"
        }
    ]
} };
});