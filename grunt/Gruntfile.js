/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
      ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
    // Task configuration.
    clean: {
      options: {
        force: true // Use with caution since traversing to external folder
      },
      build: ['../release/*', '../deploy/*']
    },
    copy: {
      main: {
        files: [
          //{src: ['src/*'], dest: 'release/', filter: 'isFile'}
          //, // includes files in path
          //{src: ['app/**'], dest: 'release/'}, // includes files in path and its subdirs
          //{expand: true, cwd: 'src/', src: '**', dest: 'release/'}
          {expand: true, cwd: '../src/', src: '*', dest: '../release/', filter: 'isFile'}
          //, // makes all src relative to cwd
          //{expand: true, flatten: true, src: ['app/**'], dest: 'dest/', filter: 'isFile'} // flattens results to a single level
        ]
      }
    },
    useref: {
      // specify which files contain the build blocks
      html: '../release/index.html',
      // explicitly specify the temp directory you are working in
      // this is the the base of your links ( "/" )
      temp: '../release'
    },
    requirejs: {
      compile: { // could be 'production', 'test', or even 'development'
        options: {
          baseUrl: './js',
          appDir: '../src/assets',
          shim: {
            underscore: {
              exports: '_'
            },
            backbone: {
              deps: [
                'underscore',
                'jquery'
              ],
              exports: 'Backbone'
            }
          },
          paths: {
            backbone: 'lib/vendor/backbone',
            domReady: 'lib/vendor/domReady',
            jquery: 'lib/vendor/jquery',
            modernizr: 'lib/vendor/modernizr-2.6.2.min',
            templates: 'templates',
            text: 'lib/vendor/text',
            underscore: 'lib/vendor/underscore.min'
          },
          dir: '../release/assets-<%= pkg.release %>',
          mainConfigFile:'../src/assets/js/main.js',
          modules: [
            {
              name: 'main'
            }
          ],
          optimize: 'uglify2' //default = uglify, others = uglify2, none
          ,optimizeCss: 'standard'
          ,removeCombined: true
        }
      }
    },
    strip: {
      main: {
        src: '../release/assets-<%= pkg.release %>/js/main.js',
        options: {
          inline: true // https://npmjs.org/package/grunt-strip
        }
      }
    },
    jshint: {
      options: {
        force: true
      },
      beforeconcat: [
        '../src/assets/js/main.js',
        '../src/assets/js/models/*.js',
        '../src/assets/js/collections/*.js',
        '../src/assets/js/views/*.js',
        '../src/assets/js/lib/*.js'
      ]
    },
    cssmin: {
      options: {
        report: 'min'
      },
      minify: {
        files: {
          '../release/assets-<%= pkg.release %>/css/indy.min.css': ['../src/assets/css/normalize.css', '../src/assets/css/journey.css']
        }
      }
    },
    compress: {
      tar: {
        options: {
          archive: '../deploy/archive.tar'
        },
        files: [
          //{src: ['path/*'], dest: 'internal_folder/', filter: 'isFile'}, // includes files in path
          //{src: ['../release/**'], dest: '../deploy/'}
          //, // includes files in path and its subdirs
          {expand: true, cwd: '../release/', src: ['**/*']}
          //, // makes all src relative to cwd
          //{flatten: true, src: ['path/**'], dest: 'internal_folder4/', filter: 'isFile'} // flattens results to a single level
        ]
      }
    },
    yuidoc: {
      compile: {
        name: '<%= pkg.name %>',
        description: '<%= pkg.description %>',
        version: '<%= pkg.version %>',
        url: '<%= pkg.homepage %>',
        options: {
          paths: '../src/',
          outdir: '../yuidocs/'
        }
      }
    },
    watch: {
      gruntfile: {
        files: '<%= jshint.gruntfile.src %>',
        tasks: ['jshint:gruntfile']
      },
      lib_test: {
        files: '<%= jshint.lib_test.src %>',
        tasks: ['jshint:lib_test', 'qunit']
      }
    }
  });

  // These plugins provide necessary tasks.
  
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-useref');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-nodeunit');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-strip');
  grunt.loadNpmTasks('grunt-contrib-compress');
  grunt.loadNpmTasks('grunt-contrib-yuidoc');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task.
  grunt.registerTask('default', ['jshint', 'clean', 'requirejs', 'strip', 'copy', 'cssmin', 'useref', 'compress', 'yuidoc']);
};
