/*jslint indent: 2, vars: true, passfail: false */
/*globals INDY, INDYmapStyle, google, document, window, event, alert, console, setTimeout */

/**
 * The main application code. See Google API for help: https://developers.google.com/maps/documentation/javascript/overlays
 * @namespace INDY
 * @class app
*/
define([
  'jquery',
  'underscore',
  'backbone',
  'collections/locationCollection',
  'views/locationView',
  'text!templates/app.html',
  'modules/util',
  'config',
  'modules/gmap',
  'modules/draw'
  ], function ($, _, backbone, Journey, LocationView, appTemplate, UTIL) {
    'use strict';

    console.log('util = ' + UTIL);

    INDY.config.updatingModel = false;

    var AppView = Backbone.View.extend({

      el: '#' + INDY.config.id, // Bind to an existing dom element

      template: _.template(appTemplate),

      events: {
        'click .build-route': 'build',
        'route-ready': 'draw',
        'click .reset-journey': 'reset'
      },

      initialize: function () {
        //console.log('Initializing AppView');
        this.render();
        INDY.GMap.initializeGmap();
        this.listenTo(Journey, 'add reset', this.addAll);
        this.listenTo(Journey, 'change', this.checkForPending);
        this.reset();
      },

      render: function () {
        //console.log('Rendering AppView');
        this.$el.append(this.template);
      },

      waitingToDraw: false,

      checkForPending: function () {
        console.log('checkForPending');
        INDY.config.updatingModel = false;
        if (this.waitingToDraw) {
          this.waitingToDraw = false;
          this.build();
        }
      },

      addLocation: function (location) {
        //console.log('Heard add');
        var view = new LocationView({model: location});
        $('#journey').append(view.render().el);
      },

      // Add all items in the Journey collection at once.
      addAll: function () {
        $('#journey').html('');
        Journey.each(this.addLocation, this);
        UTIL.selectField();
      },

      build: function () {
        var center,
          latlng,
          pixels;

        if (INDY.config.updatingModel) {
          this.waitingToDraw = true;
          console.log('waiting to draw');
          return;
        }

        _.each(Journey.models, function (model) {
          if (model.get('latlng')) {
            latlng = model.get('latlng');
            center = INDY.GMap.center(latlng[0], latlng[1]),
            pixels = INDY.GMap.pixelPosition(center);
            model.set({'x': pixels.x, 'y': pixels.y}, {silent: true});
          }
        });
        this.$el.trigger('route-ready');
      },

      draw: function () {
        INDY.draw.route();
      },

      reset: function () {
        Journey.reset(INDY.config.defaultLocations);
        INDY.draw.clearCanvas();        
      }

    });

    return AppView;
});
