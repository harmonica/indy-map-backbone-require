
/*global define*/
define([
  'underscore',
  'backbone'
], function (_, Backbone) {
  'use strict';

  var LocationModel = Backbone.Model.extend({
    
    // Default attributes for the location
    defaults: {
      placeholder: 'Please enter a destination',
      name: '',
      status: 'editing'
    },

    initialize: function () {},

    /**
     * @method geoCode
     * @description Use Google Map API to geocode the location and add result to paths and geocodes arrays.
     * @param {String} location The name of the location
     * @param {Int} i The index of the location we're dealing with
    */
    geoCode: function (location) {
      var geocoder = new google.maps.Geocoder(),
        self = this;

      geocoder.geocode({address: location}, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
          var center = results[0].geometry.location;
          self.set({name: location, latlng: [center.lat(), center.lng()], status: 'saved'});
        } else {
          //alert('Sorry, location "' + location + '" not found.');
        }
      });
    }
  });

  return LocationModel;
});