/**
 * Indiana Jones Travel By Map
 *
 * Route drawing methods
 * @namespace INDY
 * @class draw
*/

var INDY = window.INDY || {};

define([
  'jquery',
  'underscore',
  'backbone',
  'collections/locationCollection',
  'modules/util',
  'config',
  'modules/gmap'
], function ($, _, backbone, Journey, UTIL, CONFIG, GMAP) {

  INDY.draw = (function () {
    'use strict';

    /**
     * @method hypoteneuse
     * @param {Object} pathStart Reference to the starting point we will calculate the hypoteneuse from
     * @return The length of the hypoteneuse
    */
    function hypoteneuse(start) {
      var paths = Journey.models,
        A = paths[start],
        B = paths[start + 1],
        dX = B.get('x') - A.get('x'),
        dY = B.get('y') - A.get('y');
      return Math.sqrt((dX * dX) + (dY * dY));
    }

    /**
     * @method capitalize
     * @description Prototype of built-in String
     * @return String with the first character in uppercase
    */
    String.prototype.capitalize = function() {
      return this.charAt(0).toUpperCase() + this.slice(1);
    }

    /**
     * @method destinationLabel
     * @param {Object} position  Co-ordinates of destination
     * @param {Int} The leg of the overall journey we're on
    */
    function destinationLabel(position, leg) {
      var context = canvas.getContext('2d'),
        fontSize = 5.75 * map.getZoom() + 'px',
        label = Journey.models[leg].get('name'),
        x = position.x,
        y = position.y - 6;

      context.font = 'bold ' + fontSize + ' arial, sans-serif ';
      context.fillStyle = '#000';
      context.shadowOffsetX = 0;
      context.shadowOffsetY = 0;
      context.shadowBlur = 0;
      context.shadowColor = '';
      context.fillText(label.capitalize(), x, y);
    }

    /**
     * @method drawCircle
     * @description Draw a circle to denote a location
     * @param {Number} x The X co-ordinate of the circle centre
     * @param {Number} y The Y co-ordinate of the circle centre
    */
    function drawCircle(position) {
      var context = canvas.getContext('2d'),
        draw = CONFIG.draw,
        radius = draw.stroke * map.getZoom() * 1.2,
        startAngle = 0,
        endAngle = 360,
        anticlockwise = false;

      context.shadowOffsetX = draw.shadowOffsetX;
      context.shadowOffsetY = draw.shadowOffsetY;
      context.shadowBlur = draw.shadowBlur;
      context.shadowColor = draw.shadowColor;
      context.arc(position.x, position.y, radius, startAngle, endAngle, anticlockwise);
      context.fillStyle = draw.intersectionColour;
      context.fill();
    }

    /**
     * @method drawLine
     * @description Draw a line between two points on the canvas
     * @param {Object} params Object to hold the arguments such as X and Y deviation, the iteration point and legLength
    */
    function drawLine(params) {
      var context = canvas.getContext('2d'),
        draw = CONFIG.draw,
        newX = params.path.x + params.dX * params.iteration,
        newY = params.path.y + params.dY * params.iteration;

      context.shadowOffsetX = draw.shadowOffsetX;
      context.shadowOffsetY = draw.shadowOffsetY;
      context.shadowBlur = draw.shadowBlur;
      context.shadowColor = draw.shadowColor;
      context.strokeStyle = draw.routeColour;
      context.beginPath();
      context.moveTo(newX - params.dX, newY - params.dY);
      context.lineWidth = (params.dX > 0 && params.dY > 0) ? (draw.stroke * map.getZoom()) - 1 : draw.stroke * map.getZoom();
      context.lineTo(newX, newY);
      context.stroke();
      context.closePath();

      if (params.iteration === params.halfwayPoint) {
        destinationLabel(params.pathEnd, params.leg + 1); // Draw town/city label
      }

      if (params.iteration >= params.legLength) {
        console.log('End of this leg of the journey');
        setTimeout(function () {
          drawRoute(params.leg + 1);
        }, draw.speed);
      } else {
        params.iteration += 1;
        setTimeout(function () {
          drawLine(params);
        }, draw.speed);
      }
    }

    /**
     * @method drawDirectLine
     * @description Draw a line without animation. Used when zooming or moving the map
     * @param {Number} start The start point of the line
     * @param {Number} end The end point of the line
    */
    function drawDirectLine(start, end) {
      var context = canvas.getContext('2d'),
        draw = CONFIG.draw;

      context.shadowOffsetX = draw.shadowOffsetX;
      context.shadowOffsetY = draw.shadowOffsetX;
      context.shadowBlur = draw.shadowBlur;
      context.shadowColor = draw.shadowColor;
      context.strokeStyle = draw.routeColour;
      context.beginPath();
      context.moveTo(start.x, start.y);
      context.lineWidth = draw.stroke * map.getZoom();
      context.lineTo(end.x, end.y);
      context.stroke();
      context.closePath();
    }

    /**
     * @method drawRoute
     * @description Prepares the data to draw a leg of the journey
     * @param {Int} newJourney The leg of the journey we're about to draw
    */
    function drawRoute(leg) {
      var leg = leg || 0,
        paths = Journey.models,
        legLength,
        start = {},
        end = {},
        params;

      start = {x: paths[leg].get('x'), y: paths[leg].get('y')};
      
      if (leg >= paths.length - 1) {
        drawCircle(start); // End of journey so draw marker circle for city/town
        console.log('Journey ended');
        return;
      }

      end = {x: paths[leg + 1].get('x'), y: paths[leg + 1].get('y')};
      legLength = hypoteneuse(leg);

      params = {
        path: start,
        pathEnd: end,
        dX: (end.x - start.x) / (legLength + 4),
        dY: (end.y - start.y) / (legLength + 4),
        iteration: 0,
        legLength: legLength,
        leg: leg,
        halfwayPoint: parseInt(legLength * 0.75)
      };

      if (leg === 0) {
        destinationLabel(start, 0);
      }
      drawCircle(start);
      drawLine(params);
    }

    /**
     * @method reDrawRoute
     * @description Re-draw route after either zooming or moving the map
    */
    function reDrawRoute() {
      var models = Journey.models,
        i,
        max = models.length,
        start,
        end,
        center,
        nextCenter = false,
        latlng,
        nextLatlng = false,
        position,
        nextPosition = false;

      for (var i = 0; i < max; i += 1) {
        latlng = nextLatlng || models[i].get('latlng');
        center = nextCenter || GMAP.center(latlng[0], latlng[1]);
        position = nextPosition || GMAP.pixelPosition(center);
        drawCircle(position);
        if (i < max - 1) {
          nextLatlng = models[i + 1].get('latlng');
          nextCenter = GMAP.center(nextLatlng[0], nextLatlng[1]);
          nextPosition = GMAP.pixelPosition(nextCenter);
          start = {x: position.x, y: position.y};
          end = {x: nextPosition.x, y: nextPosition.y};
          drawDirectLine(start, end);
        }
        destinationLabel(position, i);
      }
    }

    /**
     * @method clearCanvas
     * @description Clears the journey from the map
    */
    function clearCanvas() {
      if (canvas) {
        canvas.width = canvas.width;
      }
    }

    return {
      route: drawRoute,
      clearCanvas: clearCanvas,
      reDrawRoute: reDrawRoute
    }
  }());

  return INDY.draw;
});