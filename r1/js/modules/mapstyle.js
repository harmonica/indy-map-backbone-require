var INDY = window.INDY || {};

INDY.mapstyle = [
    {
    "featureType": "road",
    "stylers": [
      { "visibility": "off" }
    ]
  },
  {
    "featureType": "poi",
    "stylers": [
      { "visibility": "off" }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      { "visibility": "on" }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      { "visibility": "off" }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      { "visibility": "off" }
    ]
  },
  {
    "stylers": [
      { "color": "#E9D79D" }
    ]
  },
  {
    "featureType": "administrative.country",
    "stylers": [
      { "color": "#A5833B" }
    ]
  },
  {
    "featureType": "water",
    "stylers": [
      { "color": "#FBF2C0" },
      { "gamma": 1.02 },
      { "lightness": 30 }
    ]
  },
  {
    "featureType": "landscape.man_made",
    "stylers": [
      { "visibility": "off" }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      { "visibility": "off" }
    ]
  },
  {
    "featureType": "administrative.country",
    "elementType": "labels.text.stroke",
    "stylers": [
      { "color": "#cfcecc" }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "labels.text.fill",
    "stylers": [
      { "color": "#663300" }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "labels.text",
    "stylers": [
      { "visibility": "off" }
    ]
  },
  {
    "featureType": "administrative.country",
    "stylers": [
      { "visibility": "on" },
      { "weight": 1 }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "labels.text",
    "stylers": [
      { "visibility": "off" }
    ]
  },
  {
    "featureType": "administrative.country",
    "elementType": "labels.text",
    "stylers": [
      { "visibility": "on" }
    ]
  }
];