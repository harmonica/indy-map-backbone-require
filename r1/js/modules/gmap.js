/**
 * Indiana Jones Travel By Map
 *
 * Google map methods
 * @namespace INDY
 * @class GMap
*/

var INDY = window.INDY || {};

var map,
  canvas,
  overlay;

define([
  'modules/util',
  'config'
  ], function (UTIL, CONFIG) {

  INDY.GMap = (function () {
    'use strict';

    /**
     * @method GMAPOverlay
     * @param map The Google Map that the overlay is for.
    */
    function GMAPOverlay(map) {
      this.mapOverlay = map;

      // We define a property to hold the image's div. We'll
      // actually create this div upon receipt of the onAdd()
      // method so we'll leave it null for now.
      this.divOverlay = null;
      this.setMap(map); // Call setMap on this overlay
    }

    GMAPOverlay.prototype = new google.maps.OverlayView();

    GMAPOverlay.prototype.onAdd = function () {
      // Note: an overlay's receipt of onAdd() indicates that
      // the map's panes are now available for attaching
      // the overlay to the map via the DOM.

      var div = document.createElement('div'), // Create a div to contain the canvas
        canvasElem = document.createElement('canvas'),
        gmap = CONFIG.gmap,
        panes  = this.getPanes();
      
      canvasElem.height = gmap.height;
      canvasElem.width = gmap.width;
      div.style.position = 'absolute';
      div.appendChild(canvasElem);
      this.divOverlay = div; // Set the overlay's div_ property to this DIV
      panes.overlayLayer.appendChild(div); // Add this overlay to the overlayLayer pane.
    };

    GMAPOverlay.prototype.draw = function () {
      // Size and position the overlay to cover the whole map.
      // Resize the image's DIV to fit the indicated dimensions.
      var div = this.divOverlay,
        gmap = CONFIG.gmap;
      
      canvas = overlay.divOverlay.getElementsByTagName('canvas')[0];

      div.style.left = 0;
      div.style.top = 0;
      div.style.width = gmap.width + 'px';
      div.style.height = gmap.height + 'px';
    };

    GMAPOverlay.prototype.onRemove = function () {
      this.divOverlay.parentNode.removeChild(this.divOverlay);
      this.divOverlay = null;
    };

    GMAPOverlay.prototype.alignDrawingPane = function(force) {
      window.mapProjection = this.getProjection();
      var center = window.mapProjection.fromLatLngToDivPixel(map.getCenter()),
        gmap = CONFIG.gmap,
        panes = this.getPanes();
      panes.overlayLayer.style.left = center.x - (gmap.width / 2) + 'px';
      panes.overlayLayer.style.top = center.y - (gmap.height / 2) + 'px';
    };

    /**
     * @method pixelPosition
     * @description Convert the latitude, longitude geo co-ordinates to pixels
     * @param {Object} latlng
    */
    function pixelPosition(latlng) {
      var overlayProjection = overlay.getProjection(),
        pixels = overlayProjection.fromLatLngToContainerPixel(latlng);
      return pixels;
    }

    /**
     * @method generateLatLng
     * @param {Number} lat Latitude
     * @param {Number} lon Longitude
     * @return geo co-ordinates of the location
    */
    function center(lat, lng) {
      return new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
    }

    /**
     * @method moveMap
     * @description Used by small devices to move the map on and off screen
    */
    function moveMap() {
      console.log('Moving map');
      var mapContainer = document.getElementById('map-container');
      if (mapContainer.classList.contains('small-map')) {
        mapContainer.classList.remove('small-map');
      } else {
        mapContainer.classList.add('small-map');
      }
    }

    function gmapChanged() {
      INDY.draw.clearCanvas();
      overlay.alignDrawingPane();
      INDY.draw.reDrawRoute();
    }

    /**
     * @method initializeGmap
     * @description Initialise the GoogleMap with custom options.
    */
    function initializeGmap () {
      var gmap = CONFIG.gmap,
        mapOptions = {
          zoom: gmap.zoom,
          center: gmap.center,
          mapTypeId: google.maps.MapTypeId.TERRAIN,
          mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.SATELLITE, 'map_style']
          },
          streetViewControl: gmap.streetViewControl
        };

      map = new google.maps.Map(document.getElementById('map'), mapOptions);
      map.mapTypes.set('map_style', gmap.style); // Associate the styled map with the MapTypeId
      map.setMapTypeId('map_style'); // Set it to display
      overlay = new GMAPOverlay(map);

      google.maps.event.addListener(map, "dragend", function () {
        console.log('Drag end');
        gmapChanged();
      });

      google.maps.event.addListener(map, "zoom_changed", function () {
        console.log('Zoom: ' + map.getZoom());
        gmapChanged();
      });
    }

    return {
      initializeGmap: initializeGmap,
      center: center,
      pixelPosition: pixelPosition
    }

  }());
  
  return INDY.GMap;
});