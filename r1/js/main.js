require.config({
  
  baseUrl: 'r1/js',
  
  shim: {
    underscore: {
      exports: '_'
    },
    backbone: {
      deps: [
        'underscore',
        'jquery'
      ],
      exports: 'Backbone'
    }
  },

  paths: {
    jquery: 'lib/jquery',
    underscore: 'lib/underscore.min',
    backbone: 'lib/backbone',
    domReady: 'lib/domReady',
    templates: 'templates',
    text: 'lib/text'
  }
});

require(
  [
    'domReady',
    'lib/modernizr-2.6.2.min',
    'backbone',
    'views/appView'
  ], function (domReady, modnzr, Backbone, AppView) {
    domReady(function () {
      new AppView();
    });
});