({
    baseUrl: '.',
    paths: {
      jquery: 'lib/jquery',
      underscore: 'lib/underscore.min',
      backbone: 'lib/backbone',
      domReady: 'lib/domReady',
      templates: 'templates',
      text: 'lib/text'
    },
    name: "main",
    out: "main-built.js"
})