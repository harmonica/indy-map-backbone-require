define([
  'collections/locationCollection',
  'modules/util',
  'config',
  'text!templates/location.html'
  ], function (Journey, UTIL, CONFIG, locationView) {
    'use strict';

    var LocationView = Backbone.View.extend({

      tagName: 'div',

      template: _.template(locationView),

      events: {
        'blur input': 'update',
        'click .add-location': 'add',
        'click .remove-location': 'delete',
        'remove-location': 'delete',
        'click .clear-field': 'clear'
      },

      initialize: function () {
        this.listenTo(this.model, 'destroy', this.removeView);
        this.listenTo(this.model, 'change', this.render);
        //this.listenTo(Journey, 'reset', this.removeView);
      },

      render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        this.$input = this.$('input');
        return this;
      },

      add: function (location) {
        var index = Journey.indexOf(this.model) + 1;
        UTIL.selectIndex = index;
        Journey.add({}, {at: index});
      },

      delete: function () {
        console.log('destroy location model');
        this.model.destroy({});
        this.remove();
      },

      update: function () {
        var location = this.$input.val().trim();
        if (location !== '' && location !== this.model.get('name')) {
          CONFIG.updatingModel = true; // Set flag to stop route being drawn
          this.model.set({status: 'editing'}, {silent: true});
          this.model.geoCode(location);
        }
      },

      removeView: function () {
        console.log('remove view');
        this.remove()
      },

      clear: function () {
        this.model.set({name: ''});
        this.$input.focus();
      },

      autocomplete: function () {}

    });

    return LocationView;
});