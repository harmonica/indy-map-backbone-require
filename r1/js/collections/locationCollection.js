/*global define*/
define([
  'underscore',
  'backbone',
  'models/locationModel'
], function (_, Backbone, LocationModel) {
  'use strict';

  var Journey = Backbone.Collection.extend({
    // Reference to this collection's model.
    model: LocationModel

  });

  return new Journey;
});
