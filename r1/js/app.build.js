({
    appDir: "../../",
    baseUrl: "r1/js",
    dir: "../../appdirectory-build",
    modules: [
        {
            name: "main"
        }
    ],
    paths: {
    jquery: 'lib/jquery',
    underscore: 'lib/underscore.min',
    backbone: 'lib/backbone',
    domReady: 'lib/domReady',
    templates: 'templates',
    text: 'lib/text'
  }
})