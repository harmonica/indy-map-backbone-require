# ReadMe
## Travel By Map was inspired by the sequence in the Indiana Jones films

This app was really just done for fun but also as an exercise in using the HTML canvas element with a Google Map.

# Background
The app features:
* BackboneJS,
* RequireJS,
* Google Map API,
* HTML canvas element integrated into a Google Map overlay,
* Some jQuery and vanilla JavaScript
* @font-face.

# Demo
[View a demo](http://fiveyears.co.uk/travel-by-map/)

# Documentation
[Documentation generated using YUI Doc](http://fiveyears.co.uk/travel-by-map/documentation)

# Notes
* Works best in a webkit browser i.e. Chrome, Safari,
* I've made it layout neatly on a small device e.g. iPhone but zooming and moving the map is a bit buggy,
* When time permits I'll fix the above and any other issues that come to light.

# Next (in no particular order)
* Map panning,
* Visuals e.g. an aeroplane,
* Music
